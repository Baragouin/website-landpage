import axios from 'axios';

const state = {
  user: null,
};
const actions = {
  async login({commit}, User) {
    await axios.post('login', User);
    await commit('setUser', User.get('username'));
  },
  async logout({commit}) {
    let user = null;
    commit('logout', user);
  }
};
const mutations = {
  setUser(state, username) {
    state.user = username;
  },
  logout(state) {
    state.user = null;
  }
};
const getters = {
  isAuthenticated: state => !!state.user,
  stateUser: state => state.user,
};

export default {
  state,
  getters,
  actions,
  mutations,
};