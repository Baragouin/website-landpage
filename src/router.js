import {createWebHistory, createRouter} from "vue-router";
import store from './store';

const routes = [
    {
        path: "/",
        name: "landpage",
        component: () => import("./components/LandpageLayout")
    },
    {
        path: "/admin/login",
        name: "admin-login",
        component: () => import("./components/admin/LoginForm")
    },
    {
        path: "/admin",
        name: "admin",
        meta: { requiresAuth: true },
        component: () => import("./components/admin/AdministrationLayout")
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isAuthenticated) {
            next()
            return
        }
        next('/login')
    } else {
        next()
    }
})

export default router;
