import App from './App.vue'
import { createApp, h } from 'vue'
import router from './router'
import store from './store'
import axios from 'axios'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faUserGear } from '@fortawesome/free-solid-svg-icons'
import { faAnglesDown } from '@fortawesome/free-solid-svg-icons'

library.add(faUserGear, faAnglesDown);

axios.defaults.withCredentials = true;
axios.defaults.baseURL = "http://localhost:8081/"

axios.interceptors.response.use(undefined, function (error) {
    if (error) {
        const originalRequest = error.config;
        if (error.response.status === 401 && !originalRequest._retry) {

            originalRequest._retry = true;
            store.dispatch('logout')
            return router.push('/login')
        }
    }
})


createApp({
    render: () => h(App)
})
    .use(router)
    .use(store)
    .component('fa-icon', FontAwesomeIcon)
    .mount('#app')
